Site permmettant de créer des nouveaux clients (nom, adresse mail, nom de l'entreprise et numéro de téléphone).
Après création d'un nouveau client on peut créer la demande pour un site (nom du site, lien du site, choisir un client parmit ceux créés et une version de php) 

technologies :
- Symfony
- Php
- Bootstrap
- Webpack-encore
- Sass
- Twig
- Validator

Installation : 
- composer install 
- symfony console doctrine:database:create
- symfony console doctrine:schema:update --force
- symfony -serve -d
- npm i 
- npm run watch
- Lancer le site sur l'URL : 128.0.0.1:8000