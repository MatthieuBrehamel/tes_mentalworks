<?php

namespace App\Form;

use App\Entity\Logiciel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class LogicielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class , [
                'label' => 'Nom du Logiciel : ',
            ])
            ->add('imageLogicielFile', VichImageType::class, [
                'label' => 'Image du Logiciel : ',
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer l\'image du logiciel',
                'download_label' => 'Télécharger l\'image du logiciel',
                'download_uri' => true,
                'image_uri' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Logiciel::class,
        ]);
    }
}
