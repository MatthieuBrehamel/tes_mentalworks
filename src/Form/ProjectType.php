<?php

namespace App\Form;

use App\Entity\Logiciel;
use App\Entity\Project;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomProjet', TextType::class, [
                'label' => 'Nom de votre projet : ',
                'required' => true,
            ])
            ->add('descriptionProjet', TextareaType::class, [
                'label' => 'Description de votre projet : ',
                'required' => true,
            ])
            ->add('listeObjectifs', TextareaType::class, [
                'label' => 'Objectifs de votre projet : ',
                'required' => true,
                'help' => '(Mettre "|" entre chaque objectif pour la séparation)',
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de votre projet : ',
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer l\'image',
                'download_label' => 'Télécharger l\'image',
                'download_uri' => true,
                'image_uri' => true,
            ])
            ->add('listeLogiciels', EntityType::class, [
                'class' => Logiciel::class,
                'attr' => ['class' => 'select-2'],
                'label' => 'Choisissez un/des logiciels : ',
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false,
            ])
            ->add('conclusionProjet', TextareaType::class, [
                'label' => 'Conclusion de votre projet : ',
                'required' => false,
            ])
            ->add('debutProjet', DateType::class, [
                'label' => 'Date de début de votre projet : ',
                'days'  => range(1,31),
                'months'  => range(1,12),
                'years'  => range(2000,2100),
                'format' => 'dd-MM-yyyy',
                'required' => true,
            ])
            ->add('finProjet', DateType::class, [
                'label' => 'Date de fin de votre projet : ',
                'days'  => range(1,31),
                'months'  => range(1,12),
                'years'  => range(2000,2100),
                'format' => 'dd-MM-yyyy',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
