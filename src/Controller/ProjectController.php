<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     * @param ProjectRepository $projectRepository
     * @return Response
     */
    public function index(Request $request, ProjectRepository $projectRepository): Response
    {
        $data = $request->query->get('search');

        if($data !== null) {
            $project = $projectRepository->findSiteByName($data);
        }
        else {
            $project = $projectRepository->findBy([], ['CreatedAt'=>'DESC']);
        }

        return $this->render('project/index.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @Route("/projet/{id<[0-9]+>}", name="app_project_show", methods={"GET"})
     * @param Project $project
     * @return Response
     */
    public function show(Project $project): Response
    {
        $listeObjectif = explode('|', $project->getListeObjectifs());

        return $this->render('project/show.html.twig', [
            'project' => $project,
            'listeObjectifs' => $listeObjectif,
        ]);
    }
}
