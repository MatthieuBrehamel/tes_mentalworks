<?php

namespace App\Controller;

use App\Repository\DescriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DescriptionController extends AbstractController
{
    /**
     * @Route("/description", name="description_app_home")
     */
    public function index(Request $request, DescriptionRepository $descriptionRepository)
    {
        $description = $descriptionRepository->findAll();

        return $this->render('description/index.html.twig'
            , compact('description')
        );
    }
}
