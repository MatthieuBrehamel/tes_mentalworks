<?php

namespace App\Controller\Admin;

use App\Entity\Description;
use App\Form\DescriptionType;
use App\Repository\DescriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DescriptionController extends AbstractController
{
    /**
     * @Route("/admin/description", name="admin_app_description")
     */
    public function createDescription(Request $request, EntityManagerInterface $em, DescriptionRepository $descriptionRepository)
    {
        $data = $descriptionRepository->findAll();
        if (empty($data)){
            $description = new Description();
        } else {
            $description = $data[0];
        }
        $form = $this->createForm(DescriptionType::class, $description);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($description);
                $em->flush();
                $this->addFlash('success', 'Description créée avec succès!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'ERREUR');
                dd($e);
            }

        }

        return $this->render('admin/description/edit.html.twig', [
            'description' => $description, 'form' => $form->createView(),
        ]);
    }
}
