<?php

namespace App\Controller\Admin;

use App\Entity\Cv;
use App\Form\CvType;
use App\Repository\CvRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CvController extends AbstractController
{
    /**
     * @Route("admin/cv", name="admin_app_cv")
     */
    public function index(Request $request, EntityManagerInterface $em, CvRepository $cvRepository)
    {
        $data = $cvRepository->findAll();
        if (empty($data)){
            $cv = new Cv();
        } else {
            $cv = $data[0];
        }

        $form = $this->createForm(CvType::class, $cv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $file = $cv->getName();
                $fileName = 'CV' . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory'), $fileName);
                $cv->setName($fileName);

                $em->persist($cv);
                $em->flush();
                $this->addFlash('success', 'Cv ajouté avec succès!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'ERREUR');
                dd($e);
            }

        }

        return $this->render('admin/cv/index.html.twig', [
            'form' => $form->createView(),
            'cv' => $cv,
        ]);
    }
}
