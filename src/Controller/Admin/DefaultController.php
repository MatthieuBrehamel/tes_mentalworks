<?php

namespace App\Controller\Admin;

use App\Entity\Description;
use App\Entity\Project;
use App\Form\DescriptionType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_app_home")
     */
    public function index(Request $request, ProjectRepository $projectRepository)
    {
        $data = $request->query->get('search');

        if($data !== null) {
            $project = $projectRepository->findSiteByName($data);
        }
        else {
            $project = $projectRepository->findBy([], ['CreatedAt'=>'DESC']);
        }

        return $this->render('admin/project/index.html.twig'
            , compact('project')
        );
    }

    /**
     * @Route("/admin/project/{id<[0-9]+>}", name="admin_app_project_show", methods={"GET"})
     * @param Project $project
     * @return Response
     */
    public function show(Project $project): Response
    {
        $listeObjectif = explode('|', $project->getListeObjectifs());

        return $this->render('admin/project/show.html.twig', [
            'project' => $project,
            'listeObjectifs' => $listeObjectif,
        ]);
    }

    /**
     * @Route("/admin/project/{id<[0-9]+>}/edit", name="admin_app_project_edit", methods={"GET", "PUT"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Project $project
     *
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $em, Project $project): Response
    {
        $form = $this->createForm(ProjectType::class, $project, ['method'=>'PUT']);
        $form->handleRequest($request);/*recup données formulaire*/

        if ($form->isSubmitted() && $form->isValid()) /* formulaire valide et soumis*/
        {
            $em->flush();

            $this->addFlash('success','Projet modifié avec succès!');

            return $this->redirectToRoute('admin_app_project_show', ['id' => $project->getId()]);/* redirection vers la page du pin*/
        }

        return $this->render('admin/project/edit.html.twig',[ 'project' => $project, 'form' => $form->createView()]);
    }

    /**
     * @Route("/admin/project/create", name="admin_app_project_create")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createSite(Request $request, EntityManagerInterface $em)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($project);
                $em->flush();
                $this->addFlash('success', 'Projet créé avec succès!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'ERREUR');
                dd($e);
            }

            return $this->redirectToRoute('admin_app_project_show', ['id' => $project->getId()]);
        }

        return $this->render('admin/project/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/project/{id<[0-9]+>}",name="admin_app_project_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EntityManagerInterface $em, Project $project): Response
    {
        if($this->isCsrfTokenValid('project_deletion' . $project->getId(), $request->request->get('csrf_token'))) {
            $em->remove($project);
            $em->flush();

            $this->addFlash('info','Projet supprimé avec succès!');
        }
        return $this->redirectToRoute('admin_app_home');
    }
}
