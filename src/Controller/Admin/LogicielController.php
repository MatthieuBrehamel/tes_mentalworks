<?php

namespace App\Controller\Admin;

use App\Entity\Logiciel;
use App\Form\LogicielType;
use App\Repository\LogicielRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogicielController extends AbstractController
{
    /**
     * @Route("/admin/logiciel", name="admin_app_logiciel_index")
     */
    public function index(Request $request, LogicielRepository $logicielRepository)
    {
        $logiciel = $logicielRepository->findBy([], ['CreatedAt'=>'DESC']);

        return $this->render('admin/logiciel/index.html.twig'
            , compact('logiciel')
        );
    }

    /**
     * @Route("/admin/logiciel/{id<[0-9]+>}/edit", name="admin_app_logiciel_edit", methods={"GET", "PUT"})
     *
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $em, Logiciel $logiciel): Response
    {
        $form = $this->createForm(LogicielType::class, $logiciel, ['method'=>'PUT']);
        $form->handleRequest($request);/*recup données formulaire*/

        if ($form->isSubmitted() && $form->isValid()) /* formulaire valide et soumis*/
        {
            $em->flush();

            $this->addFlash('success','Logiciel modifié avec succès!');

        }

        return $this->render('admin/logiciel/edit.html.twig',[ 'logiciel' => $logiciel, 'form' => $form->createView()]);
    }

    /**
     * @Route("/admin/logiciel/create", name="admin_app_logiciel_create")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function createSite(Request $request, EntityManagerInterface $em)
    {
        $logiciel = new Logiciel();
        $form = $this->createForm(LogicielType::class, $logiciel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($logiciel);
                $em->flush();
                $this->addFlash('success', 'Projet créé avec succès!');
            } catch (\Exception $e) {
                $this->addFlash('error', 'ERREUR');
                dd($e);
            }

            return $this->redirectToRoute('admin_app_logiciel_index');
        }

        return $this->render('admin/logiciel/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/logiciel/{id<[0-9]+>}",name="admin_app_logiciel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EntityManagerInterface $em, Logiciel $logiciel): Response
    {
        if($this->isCsrfTokenValid('logiciel_deletion' . $logiciel->getId(), $request->request->get('csrf_token'))) {
            $em->remove($logiciel);
            $em->flush();

            $this->addFlash('info','Logiciel supprimé avec succès!');
        }
        return $this->redirectToRoute('admin_app_logiciel_index');
    }
}
