<?php

namespace App\Controller;

use App\Repository\CvRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CvController extends AbstractController
{
    /**
     * @Route("/cv", name="cv_app_home")
     */
    public function index(Request $request, CvRepository $cvRepository)
    {
        $cv = $cvRepository->findAll();

        return $this->render('cv/index.html.twig'
            , compact('cv')
        );
    }
}
