<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @Vich\Uploadable
 * @ORM\Table(name="project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le nom du projet ne peut pas être vide")
     */
    private $nomProjet;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="La description du projet ne peut pas être vide")
     */
    private $descriptionProjet;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="La liste d'objectifs du projet ne peut pas être vide")
     */
    private $listeObjectifs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Logiciel")
     */
    private $listeLogiciels;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conclusionProjet;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="La date de début du projet ne peut pas être vide")
     */
    private $debutProjet;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $finProjet;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * maxSize = "10M",
     * mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     * maxSizeMessage = "La taille maximum autorisée est de 5MB.",
     *
     * @Vich\UploadableField(mapping="projets", fileNameProperty="imageName")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $CreatedAt;

    public function __construct() {
        $this->listeLogiciels = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNomProjet(): ?string
    {
        return $this->nomProjet;
    }

    /**
     * @param string $nomProjet
     * @return $this
     */
    public function setNomProjet(string $nomProjet): self
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescriptionProjet()
    {
        return $this->descriptionProjet;
    }

    /**
     * @param mixed $descriptionProjet
     */
    public function setDescriptionProjet($descriptionProjet): void
    {
        $this->descriptionProjet = $descriptionProjet;
    }

    /**
     * @return mixed
     */
    public function getListeObjectifs()
    {
        return $this->listeObjectifs;
    }

    /**
     * @param mixed $listeObjectifs
     */
    public function setListeObjectifs($listeObjectifs): void
    {
        $this->listeObjectifs = $listeObjectifs;
    }



    public function getListeLogiciels()
    {
        return $this->listeLogiciels;
    }

    public function addListeLogiciels(Project $sites)
    {
        if ($this->listeLogiciels->contains($sites)) {
            return;
        }
        $this->listeLogiciels[] = $sites;
    }

    public function removeListeLogiciels(Project $sites)
    {
        if (!$this->listeLogiciels->contains($sites)) {
            return;
        }
        $this->listeLogiciels->removeElement($sites);
    }

    public function getConclusionProjet(): ?string
    {
        return $this->conclusionProjet;
    }

    public function setConclusionProjet(?string $conclusionProjet): self
    {
        $this->conclusionProjet = $conclusionProjet;

        return $this;
    }

    public function getDebutProjet(): ?\DateTimeInterface
    {
        return $this->debutProjet;
    }

    public function setDebutProjet(\DateTimeInterface $debutProjet): self
    {
        $this->debutProjet = $debutProjet;

        return $this;
    }

    public function getFinProjet(): ?\DateTimeInterface
    {
        return $this->finProjet;
    }

    public function setFinProjet(?\DateTimeInterface $finProjet): self
    {
        $this->finProjet = $finProjet;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    /**
     * @param \DateTimeInterface $CreatedAt
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->setCreatedAt(new \DateTimeImmutable());
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
}
