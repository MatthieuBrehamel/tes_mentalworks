<?php

namespace App\Entity;

use App\Repository\LogicielRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=LogicielRepository::class)
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class Logiciel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * maxSize = "10M",
     * mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     * maxSizeMessage = "La taille maximum autorisée est de 5MB.",
     *
     * @Vich\UploadableField(mapping="projets", fileNameProperty="imageLogicielName")
     *
     * @var File|null
     */
    private $imageLogicielFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $imageLogicielName;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $CreatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    /**
     * @param \DateTimeInterface $CreatedAt
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->setCreatedAt(new \DateTimeImmutable());
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageLogicielFile
     */
    public function setImageLogicielFile(?File $imageLogicielFile = null): void
    {
        $this->imageLogicielFile = $imageLogicielFile;

        if (null !== $imageLogicielFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageLogicielFile(): ?File
    {
        return $this->imageLogicielFile;
    }

    public function setImageLogicielName(?string $imageLogicielName): void
    {
        $this->imageLogicielName = $imageLogicielName;
    }

    public function getImageLogicielName(): ?string
    {
        return $this->imageLogicielName;
    }
}
