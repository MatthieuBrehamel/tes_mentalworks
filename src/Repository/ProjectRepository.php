<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findSiteByName(string $name): array
    {
        $queryBuilder = $this->createQueryBuilder('project');

        $queryBuilder
            ->where('project.nomProjet LIKE :name')
            ->setParameter('name', '%'. $name . '%')
            ->orderBy('project.CreatedAt', 'DESC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
